<?php


namespace App\Model\Api;


use Symfony\Component\Validator\Constraints as Assert;

class UserFollowingApiModel
{
    /**
     * @Assert\NotBlank()
     */
    private $userFollowed;

    public function getUserFollowed()
        {
        return $this->userFollowed;
        }

    public function setUserFollowed($userFollowed): void
        {
        $this->userFollowed = trim($userFollowed);
        }
}