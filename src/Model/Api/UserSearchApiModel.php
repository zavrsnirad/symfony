<?php


namespace App\Model\Api;


use Symfony\Component\Validator\Constraints as Assert;

class UserSearchApiModel
{
    /**
     * @Assert\NotBlank()
     */
    private $searchTerms;

    public function getSearchTerms()
        {
        return $this->searchTerms;
        }

    public function setSearchTerms($searchTerms): void
        {
        $this->searchTerms = trim($searchTerms);
        }
}