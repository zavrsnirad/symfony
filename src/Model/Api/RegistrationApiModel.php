<?php


namespace App\Model\Api;


use Symfony\Component\Validator\Constraints as Assert;

class RegistrationApiModel
{
    /**
     * @Assert\NotBlank(
     *     message="Username can't be empty!",
     * )
     * @Assert\Length(
     *     min="6",
     *     max="20",
     *     minMessage="Username must be at least {{ limit }} characters long!",
     *     maxMessage="Username can't be longer than {{ limit }} characters!"
     * )
     */
    private $username;

    /**
     * @Assert\NotBlank(
     *     message="Email can't be empty!"
     * )
     * @Assert\Email(
     *     message="Email {{ value }} is not a valid email address!"
     * )
     */
    private $email;

    /**
     * @Assert\NotBlank(
     *     message="Password can't be empty!",
     * )
     * @Assert\Length(
     *     min="8",
     *     max="20",
     *     minMessage="Password must be at least {{ limit }} characters long!",
     *     maxMessage="Password can't be longer than {{ limit }} characters!"
     * )
     */
    private $plainPassword;

    /**
     * @Assert\NotBlank(message="Confirm password can't be empty")
     * @Assert\Length(
     *     min="8",
     *     max="20",
     *     minMessage="Confirm password must be at least {{ limit }} characters long!",
     *     maxMessage="Confirm password can't be longer than {{ limit }} characters!"
     * )
     * @Assert\EqualTo(
     *     propertyPath="plainPassword",
     *     message="Password mismatch!"
     * )
     */
    private $plainConfirmPassword;

    public function getUsername()
        {
        return $this->username;
        }

    public function setUsername($username): void
        {
        $this->username = trim($username);
        }

    public function getEmail()
        {
        return $this->email;
        }

    public function setEmail($email): void
        {
        $this->email = trim($email);
        }

    public function getPlainPassword()
        {
        return $this->plainPassword;
        }

    public function setPlainPassword($plainPassword): void
        {
        $this->plainPassword = trim($plainPassword);
        }

    public function getPlainConfirmPassword()
        {
        return $this->plainConfirmPassword;
        }

    public function setPlainConfirmPassword($plainConfirmPassword): void
        {
        $this->plainConfirmPassword = $plainConfirmPassword;
        }
}