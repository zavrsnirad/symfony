<?php


namespace App\Model\Api;


use Symfony\Component\Validator\Constraints as Assert;

class CalendarApiModel
{
    /**
     * @Assert\NotBlank(
     *     message="Let us know did you do your workout"
     * )
     * @Assert\Choice(
     *     callback={"App\Model\Api\CalendarApiModel", "getValidValues"},
     *     message="Please select a valid value"
     * )
     */
    private $isWorkoutDone;

    public function getIsWorkoutDone()
        {
        return $this->isWorkoutDone;
        }

    public function setIsWorkoutDone($isWorkoutDone): void
        {
        $this->isWorkoutDone = $isWorkoutDone;
        }

    public function getValidValues()
        {
        return [true, 1, false, 0];
        }
}