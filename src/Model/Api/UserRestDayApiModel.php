<?php


namespace App\Model\Api;


use Symfony\Component\Validator\Constraints as Assert;

class UserRestDayApiModel
{
    /**
     * @Assert\NotBlank()
     * @Assert\Choice(
     *     callback={"App\Model\Api\UserRestDayApiModel", "getValidValues"},
     *     message="Please select a valid day"
     * )
     */
    private $restDay;

    public function getRestDay()
        {
        return $this->restDay;
        }

    public function setRestDay($restDay): void
        {
        $this->restDay = trim($restDay);
        }

    public static function getValidValues()
        {
        return ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        }
}