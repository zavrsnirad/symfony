<?php


namespace App\Model\Api;


use Symfony\Component\Validator\Constraints as Assert;

class LoginApiModel
{
    /**
     * @Assert\NotBlank(
     *     message="Username can't be empty!"
     * )
     * @Assert\Length(
     *     min="6",
     *     max="20",
     *     minMessage="Username must be at least {{ limit }} characters long!",
     *     maxMessage="Username can't be longer than {{ limit }} characters!"
     * )
     */
    private $username;

    /**
     * @Assert\NotBlank(
     *     message="Password can't be empty!"
     * )
     * @Assert\Length(
     *     min="8",
     *     max="20",
     *     minMessage="Password must be at least {{ limit }} characters long!",
     *     maxMessage="Password can't be longer than {{ limit }} characters!"
     * )
     */
    private $plainPassword;

    public function getUsername()
        {
        return $this->username;
        }

    public function setUsername($username): void
        {
        $this->username = trim($username);
        }

    public function getPlainPassword()
        {
        return $this->plainPassword;
        }

    public function setPlainPassword($plainPassword): void
        {
        $this->plainPassword = trim($plainPassword);
        }
}