<?php


namespace App\Service;


use Gedmo\Sluggable\Util\Urlizer;
use League\Flysystem\FileExistsException;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderHelper
{
    const ALL_IMAGES_DIRECTORY = 'images';
    const USER_IMAGES = 'user_images';

    private $filesystem;

    public function __construct(FilesystemInterface $publicUploadsFilesystem)
        {
        $this->filesystem = $publicUploadsFilesystem;
        }


    public function uploadUserImage(File $file, int $userId): string
        {
        try
            {
            $newFileName = $this->uploadFile($file, self::USER_IMAGES . DIRECTORY_SEPARATOR . $userId);
            return $newFileName;
            }
        catch (\Exception $e)
            {
            return $e->getMessage();
            }
        }

    private function uploadFile(File $file, string $directory): string
        {
        if ($file instanceof UploadedFile)
            {
            $originalFileName = $file->getClientOriginalName();
            }
        else
            {
            $originalFileName = $file->getFilename();
            }

        $newFileName = Urlizer::urlize(pathinfo($originalFileName, PATHINFO_FILENAME)) . '-' . time() . '-' . uniqid() . '.' . $file->guessExtension();

        $stream = fopen($file->getPathname(), 'r');

        try
            {
            $result = $this->filesystem->writeStream(
                $directory . DIRECTORY_SEPARATOR . $newFileName,
                $stream
            );

            if ($result === false)
                {
                throw new \Exception(sprintf('Could not write uploaded file "%s"', $newFileName));
                }

            if (is_resource($stream))
                {
                fclose($stream);
                }

            return $newFileName;
            }
        catch (FileExistsException $e)
            {
            return $e->getMessage();
            }
        }
}