<?php


namespace App\Controller\Web;


use App\Controller\BaseController;
use App\Entity\Calendar;
use App\Entity\UserRestDay;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends BaseController
{

    /**
     * @Route("/", name="home_page")
     */
    public function indexAction()
        {
        if ($this->getUser() != null)
            {
            $calendarCount = 0;
            $restDay = 'You don\'t have an active rest day!';
            $calendar = $this->entityManager->getRepository(Calendar::class)->getUserCalendar($this->getUser()->getUsername());
            if ($calendar != null)
                {
                foreach ($calendar as $item)
                    {
                    if ($item['isWorkoutDone'])
                        {
                        $calendarCount++;
                        }
                    }
                }

            $activeRestDay = $this->entityManager
                ->getRepository(UserRestDay::class)
                ->findActiveRestDay($this->getUser()->getId());

            if ($activeRestDay)
                {
                $restDay = 'You current rest day is ';
                switch ($activeRestDay[0]['restDay'])
                    {
                    case 'Mon':
                        $restDay .= 'Monday and it is active until ';
                        break;
                    case 'Tue':
                        $restDay .= 'Tuesday and it is active until ';
                        break;
                    case 'Wed':
                        $restDay .= 'Wednesday and it is active until ';
                        break;
                    case 'Thu':
                        $restDay .= 'Thursday and it is active until ';
                        break;
                    case 'Fri':
                        $restDay .= 'Friday and it is active until ';
                        break;
                    case 'Sat':
                        $restDay .= 'Saturday and it is active until ';
                        break;
                    case 'Sun':
                        $restDay .= 'Sunday and it is active until ';
                        break;
                    }

                $activeTo = 'you set a new rest day.';
                if ($activeRestDay[0]['activeTo'] instanceof \DateTime)
                    {
                    $activeTo = $activeRestDay[0]['activeTo']->format('d.m.Y.');
                    }

                $restDay .= $activeTo;
                }


            return $this->render('web/index.html.twig', [
                'calendarCount' => $calendarCount,
                'userRegistrationDate' => $this->getUser()->getCreatedAt(),
                'restDay' => $restDay
            ]);
            }

        return $this->render('web/index.html.twig');
        }

    /**
     * @Route("/download_app", name="download_app")
     */
    public function downloadAction()
        {
        return $this->file('downloads/app-debug.apk', 'workoutposse.apk');
        }

}