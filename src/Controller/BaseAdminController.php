<?php


namespace App\Controller;


use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class BaseAdminController extends EasyAdminController
{
    public function redirectToAdminEntity($entity)
        {
        return $this->redirect($this->generateUrl('easyadmin', [
            'entity' => $entity
        ]));
        }
}