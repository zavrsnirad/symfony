<?php


namespace App\Controller\Api;


use App\Controller\BaseController;
use App\Entity\User;
use App\Entity\UserFollowing;
use App\Model\Api\UserFollowingApiModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserFollowingController
 * @package App\Controller\Api
 * @Route("/api/auth/user")
 */
class UserFollowingController extends BaseController
{
    /**
     * @Route("/following/{username}", name="api_get_following", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getFollowingUsers(Request $request)
        {
        $username = $request->get('username');

        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['username' => $username]);

        if (!$user)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_DATA,
                'User doesn\'t exist'
            );
            }

        $following = $this->entityManager->getRepository(UserFollowing::class)
            ->getFollowing($user->getId());

        if (!$following)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_FOLLOWING,
                'Didn\'t get user following'
            );
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_USER_FOLLOWING,
            'Got user following',
            null,
            $following
        );
        }

    /**
     * @Route("/followers/{username}", name="api_get_followers", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getFollowers(Request $request)
        {
        $username = $request->get('username');

        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['username' => $username]);

        if (!$user)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_DATA,
                'User doesn\'t exist'
            );
            }

        $followers = $this->entityManager->getRepository(UserFollowing::class)
            ->getFollowers($user->getId());

        if (!$followers)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_FOLLOWERS,
                'Didn\'t get user followers'
            );
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_USER_FOLLOWERS,
            'Got user followers',
            null,
            $followers
        );
        }

    /**
     * @Route("/follow", name="api_follow_user", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function followUser(Request $request)
        {
        $userFollowingApiModel = $this->deserializeToJson(
            $request->getContent(),
            UserFollowingApiModel::class
        );

        $userToFollow = $this->entityManager->getRepository(User::class)
            ->findOneBy(['id' => $userFollowingApiModel->getUserFollowed()]);

        if (!$userToFollow)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_DATA,
                'User doesn\'t exist'
            );
            }

        $alreadyFollowed = $this->entityManager->getRepository(UserFollowing::class)
            ->findOneBy([
                'userFollowing' => $this->getUser(),
                'userFollowed' => $userToFollow
            ]);

        if ($alreadyFollowed)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_FOLLOWED,
                'User already followed'
            );
            }

        $userFollowing = new UserFollowing();
        $userFollowing->setUserFollowing($this->getUser());
        $userFollowing->setUserFollowed($userToFollow);
        $this->entityManager->persist($userFollowing);
        $this->entityManager->flush();

        return $this->createApiJsonResponse(
            self::SUCCESS_USER_FOLLOWED,
            'User followed'
        );
        }

    /**
     * @Route("/unfollow/{userId}", name="api_unfollow_user", methods={"DELETE"})
     * @param Request $request
     * @return JsonResponse
     */
    public function unFollowUser(Request $request)
        {
        $userId = $request->get('userId');

        $userToUnfollow = $this->entityManager->getRepository(User::class)
            ->findOneBy(['id' => $userId]);

        if (!$userToUnfollow)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_DATA,
                'User doesn\'t exist'
            );
            }

        $followed = $this->entityManager->getRepository(UserFollowing::class)
            ->findOneBy([
                'userFollowing' => $this->getUser(),
                'userFollowed' => $userToUnfollow
            ]);

        if (!$followed)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_UNFOLLOWED,
                'User not followed'
            );
            }

        $this->entityManager->remove($followed);
        $this->entityManager->flush();

        return $this->createApiJsonResponse(
            self::SUCCESS_USER_UNFOLLOWED,
            'User unfollowed'
        );
        }

}