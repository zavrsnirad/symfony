<?php


namespace App\Controller\Api;



use App\Controller\BaseController;
use App\Entity\User;
use App\Entity\UserFollowing;
use App\Entity\UserImage;
use App\Service\UploaderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\File\File as FileObject;

/**
 * Class UserImageController
 * @package App\Controller\Api
 * @Route("/api/auth/user")
 */
class UserImageController extends BaseController
{
    private $uploaderHelper;
    const UPLOAD_ROOT = 'uploads/user_images/';

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        UploaderHelper $uploaderHelper)
        {
        parent::__construct($serializer, $validator, $entityManager, $passwordEncoder);
        $this->uploaderHelper = $uploaderHelper;
        }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/image_upload", name="api_user_image_upload", methods={"POST"})
     */
    public function uploadUserImage(Request $request)
        {
        $file = $request->files->get('imageFile');
        $fileName = $request->get('imageFileName');
        $isProfileImage = $request->get('isProfileImage');
        $description = $request->get('description');

        if ($isProfileImage)
            {
            $this->entityManager
                ->getRepository(UserImage::class)
                ->createQueryBuilder('ui')
                ->update(UserImage::class, 'ui')
                ->set('ui.isProfileImage', 0)
                ->set('ui.isOldProfileImage', 1)
                ->andWhere('ui.user = :user AND ui.isProfileImage = :isProfileImage')
                ->setParameter('user', $this->getUser()->getId())
                ->setParameter('isProfileImage', 1)
                ->getQuery()
                ->execute();
            }

        $uploadedFile = new FileObject($file);
        $originalFileName = $fileName;

        $violations = $this->validator->validate(
            $uploadedFile,
            [
                new File([
                    'maxSize' => '5M',
                    'mimeTypes' => ['image/jpeg', 'image/jpg', 'image/png'],
                    'mimeTypesMessage' => 'Image must be PNG or JPEG format'
                ]),
                new NotBlank([
                    'message' => 'Please select a file to upload'
                ])
            ]
        );

        if ($violations->count() > 0)
            {
            return $this->validationErrorResponse($violations, 400);
            }

        $imageFileName = $this->uploaderHelper->uploadUserImage($uploadedFile, $this->getUser()->getId());
        $imagePath = self::UPLOAD_ROOT . $this->getUser()->getId() . "/" . $imageFileName;

        if (file_exists($imagePath))
            {
            $userImage = new UserImage($this->getUser());
            $userImage->setImageFileName($imagePath);
            $userImage->setOriginalImageFileName($originalFileName ?? $imageFileName);
            $userImage->setIsProfileImage($isProfileImage);
            $userImage->setDescription($description != '' ? $description : null);

            if (is_file($uploadedFile->getPathname()))
                {
                unlink($uploadedFile->getPathname());
                }

            $this->entityManager->persist($userImage);
            $this->entityManager->flush();

            $user = [
                'userId' => $this->getUser()->getId(),
                'username' => $this->getUser()->getUsername(),
            ];

            return $this->createApiJsonResponse(
                self::SUCCESS_IMAGE_UPLOAD,
                'Image uploaded successfully',
                $user,
                [
                    'imageFileName' => $userImage->getImageFileName(),
                    'description' => $userImage->getDescription(),
                    'isProfileImage' => $userImage->getIsProfileImage(),
                    'createdAt' => $userImage->getCreatedAt()
                ]
            );
            }

        return $this->createApiJsonResponse(
            self::ERROR_IMAGE_UPLOAD,
            'Image wasn\'t uploaded'
        );
        }


    /**
     * @Route("/user_images/{username}", name="api_get_user_images", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserImages(Request $request)
        {
        $user = $this->entityManager
            ->getRepository(User::class)
            ->getUserData($request->get('username'));

        if (!$user)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_DATA,
                'This user does not exist'
            );
            }

        $profileImage = $this->entityManager
            ->getRepository(UserImage::class)
            ->getUserProfileImage($request->get('username'));

        $images = $this->entityManager
            ->getRepository(UserImage::class)
            ->getAllUserImages($request->get('username'));

        if (!$images)
            {
            return $this->createApiJsonResponse(
                self::ERROR_GET_USER_IMAGES,
                'User currently has no images',
                $user
            );
            }

        $data = [];

        foreach ($images as $image)
            {
            if (!empty($profileImage))
                {
                $image['profileImageFileName'] = $profileImage[0]['imageFileName'];
                }
            else
                {
                $image['profileImageFileName'] = null;
                }
            $data[] = $image;
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_GET_USER_IMAGES,
            'Got images successfully',
            $user,
            $data
        );
        }

    /**
     * @Route("/user_profile_image/{username}", name="api_get_user_profile_image", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserProfileImage(Request $request)
        {
        $user = $this->entityManager
            ->getRepository(User::class)
            ->getUserData($request->get('username'));

        if (!$user)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_DATA,
                'This user does not exist'
            );
            }

        $profileImage = $this->entityManager
            ->getRepository(UserImage::class)
            ->getUserProfileImage($request->get('username'));

        if (!$profileImage)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_PROFILE_IMAGE,
                'User has no profile image',
                $user
            );
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_USER_PROFILE_IMAGE,
            'Got user profile image',
            $user,
            $profileImage
        );
        }

    /**
     * @Route("/followed_users_images", name="api_get_followed_users_images", methods={"GET"})
     */
    public function getFollowedUserImages()
        {
        $data = [];
        $followingUsersIds = [];
        $usersWithImagesIds = [];
        $profileImagesUserIds = [];
        $followingUsers = $this->entityManager->getRepository(UserFollowing::class)->getFollowing($this->getUser()->getId());

        if (!$followingUsers)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_FOLLOWING,
                'You don\'t follow anybody'
            );
            }

        foreach ($followingUsers as $user)
            {
            $followingUsersIds[] = $user['userFollowed'];
            }

        $usersWithImages = $this->entityManager->getRepository(UserImage::class)->getFollowingUserImages($followingUsersIds);

        if (!$usersWithImages)
            {
            return $this->createApiJsonResponse(
                self::ERROR_HOME_IMAGES,
                'Got no images from following users'
            );
            }

        foreach ($usersWithImages as $user)
            {
            $usersWithImagesIds[] = $user['id'];
            }

        $usersWithImagesIds = array_unique($usersWithImagesIds);

        $profileImages = $this->entityManager->getRepository(UserImage::class)->getFollowedUserProfileImage($usersWithImagesIds);

        foreach ($profileImages as $profileImage)
            {
            $profileImagesUserIds[] = $profileImage['id'];
            }

        foreach ($usersWithImages as $user)
            {
            $userIdIndex = array_search($user['id'], $profileImagesUserIds);
            if ($userIdIndex === false)
                {
                $user['profileImageFileName'] = null;
                }
                else
                {
                $user["profileImageFileName"] = $profileImages[$userIdIndex]['imageFileName'];
                }
                $data[] = $user;
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_HOME_IMAGES,
            'Got images from following users',
            null,
            $data
        );
        }

    /**
     * @Route("/delete_image", name="api_delete_image", methods={"DELETE"}, requirements={"fileName"="\fileName+"})
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteImage(Request $request)
        {
        $user = $this->getUser();
        $fileName = $request->get('fileName');

        $image = $this->entityManager->getRepository(UserImage::class)->findOneBy([
            'user' => $user,
            'imageFileName' => $fileName
        ]);

        if (!$image)
            {
            return $this->createApiJsonResponse(
                self::ERROR_IMAGE_NOT_FOUND,
                'This image was not found in database'
            );
            }

        if (file_exists($fileName))
            {
            unlink($fileName);
            $this->entityManager->remove($image);
            $this->entityManager->flush();
            return $this->createApiJsonResponse(
                self::SUCCESS_IMAGE_DELETED,
                'Image deleted successfully'
            );
            }

        return $this->createApiJsonResponse(
            self::ERROR_IMAGE_NOT_DELETED,
            'Image not deleted, file doesn\'t exist'
        );
        }
}