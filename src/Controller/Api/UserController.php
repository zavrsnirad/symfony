<?php


namespace App\Controller\Api;


use App\Controller\BaseController;
use App\Entity\Calendar;
use App\Entity\User;
use App\Entity\UserFollowing;
use App\Entity\UserImage;
use App\Entity\UserRestDay;
use App\Model\Api\ChangePasswordApiModel;
use App\Model\Api\UserSearchApiModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller\Api
 * @Route("/api/auth/user")
 */
class UserController extends BaseController
{
    /**
     * @Route("/login_data", name="api_login_user_data", methods={"GET"})
     * @return Response
     */
    public function getLoginUserData()
        {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->getLoginUserData($this->getUser()->getUsername())
        ;

        if (!$user)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_DATA,
                'There is no user'
            );
            }

        $user = [
            'id' => $user[0]['id'],
            'email' => $user[0]['email']
        ];

        return $this->createApiJsonResponse(
            self::SUCCESS_USER_DATA,
            'User data got successfully',
            $user
        );
        }

    /**
     * @Route("/search_users/{searchTerms}", name="api_search_users", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function searchUsers(Request $request)
        {
        $user = $this->getUser()->getUsername();
        $searchTerms = $request->get('searchTerms');
        $userIds = [];
        $tempSearchTerms = explode(' ', $searchTerms);
        $searchTermsArray = array_filter($tempSearchTerms);
        $searchTermsArray = array_unique($searchTermsArray);
        $searchTermsArray = array_values($searchTermsArray);

        foreach ($searchTermsArray as $searchTerm)
            {
            $userId = $this->entityManager->getRepository(User::class)
                ->searchUserIds($searchTerm, $user);

            if ($userId)
                {
                foreach ($userId as $id)
                    {
                    $userIds[] = $id['id'];
                    }
                }
            }

        if (!$userIds)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_SEARCH,
                'No users found'
            );
            }

        $userIds = array_unique($userIds);

        $users = $this->entityManager->getRepository(User::class)
            ->searchUsersByIds($userIds);

        if (!$users)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_SEARCH,
                'No users found'
            );
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_USER_SEARCH,
            'Users found in search',
            null,
            $users
        );
        }

    /**
     * @Route("/change_password", name="api_change_password", methods={"PATCH"})
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function changePassword(Request $request)
        {
        $changePasswordApiModel = $this->deserializeToJson(
            $request->getContent(),
            ChangePasswordApiModel::class
        );

        $violations = $this->validator->validate($changePasswordApiModel);

        if ($violations->count() > 0)
            {
            return $this->validationErrorResponse($violations, 400);
            }

        $user = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['id' => $this->getUser()->getId()]);

        if (!$user)
            {
            return $this->createApiJsonResponse(
                self::ERROR_CHANGE_PASSWORD,
                'This user doen\'t exist'
            );
            }

        $user->setPassword($this->passwordEncoder->encodePassword($user, $changePasswordApiModel->getPlainPassword()));
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->createApiJsonResponse(
            self::SUCCESS_CHANGE_PASSWORD,
            'Password changed successfully'
        );
        }

    /**
     * @Route("/delete_user", name="api_delete_user", methods={"DELETE"})
     */
    public function deleteUser()
        {
        $user = $this->getUser();

        $images = $this->entityManager->getRepository(UserImage::class)->findBy([
            'user' => $user
        ]);

        if ($images)
            {
            foreach ($images as $image)
                {
                if (file_exists($image->getImageFileName()))
                    {
                    unlink($image->getImageFileName());
                    }
                $this->entityManager->remove($image);
                }
            }

        $restDays = $this->entityManager->getRepository(UserRestDay::class)->findBy([
            'user' => $user
        ]);

        if ($restDays)
            {
            foreach ($restDays as $restDay)
                {
                $this->entityManager->remove($restDay);
                }
            }


        $calendarDays = $this->entityManager->getRepository(Calendar::class)->findBy([
            'user' => $user
        ]);

        if ($calendarDays)
            {
            foreach ($calendarDays as $calendarDay)
                {
                $this->entityManager->remove($calendarDay);
                }
            }

        $userFollowings = $this->entityManager->getRepository(UserFollowing::class)->findBy([
            'userFollowing' => $user
        ]);

        if ($userFollowings)
            {
            foreach ($userFollowings as $userFollowing)
                {
                $this->entityManager->remove($userFollowing);
                }
            }

        $userFollowers = $this->entityManager->getRepository(UserFollowing::class)->findBy([
            'userFollowed' => $user
        ]);

        if ($userFollowers)
            {
            foreach ($userFollowers as $userFollower)
                {
                $this->entityManager->remove($userFollower);
                }
            }

        $userData = $this->entityManager->getRepository(User::class)->findOneBy([
            'id' => $user->getId()
        ]);

        $this->entityManager->remove($userData);

        $this->entityManager->flush();

        return $this->createApiJsonResponse(
            self::DELETE_USER,
            'User deleted'
        );
        }

    /**
     * @Route("/send_notice/{userId}", name="api_send_notice", methods={"GET"})
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return JsonResponse
     */
    public function sendNoticeEmail(Request $request, \Swift_Mailer $mailer)
        {
        $sender = $this->getUser();
        $userId = $request->get('userId');

        $receiver = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $userId]);

        $message = (new \Swift_Message('Contact me!'))
            ->setFrom('workout.posse.team@gmail.com')
            ->setTo($receiver->getEmail())
            ->setBody(
                $this->renderView('emails/notice.html.twig', [
                    'receiveUsername' => $receiver->getUsername(),
                    'senderUsername' => $sender->getUsername(),
                    'senderMail' => $sender->getEmail()
                ]),
                'text/html'
            )
            ->setReadReceiptTo(['workout.posse.team@gmail.com'])
        ;
        $mailer->send($message);

        return $this->createApiJsonResponse(
            self::MAIL_SENT,
            'Email sent'
        );
        }
}