<?php


namespace App\Controller\Api;


use App\Controller\BaseController;
use App\Entity\UserRestDay;
use App\Model\Api\UserRestDayApiModel;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserRestDayController
 * @package App\Controller\Api
 * @Route("/api/auth/user")
 */
class UserRestDayController extends BaseController
{
    /**
     * @Route("/create_rest_day", name="create_user_rest_day", methods={"POST"})
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function createUserRestDayAction(Request $request)
        {
        $restDayApiModel = $this->deserializeToJson(
            $request->getContent(),
            UserRestDayApiModel::class);

        $violations = $this->validator->validate($restDayApiModel);

        if ($violations->count() > 0)
            {
            return $this->validationErrorResponse($violations, 400);
            }

        $restDay = $this->entityManager
            ->getRepository(UserRestDay::class)
            ->findOneBy(
                ['user' => $this->getUser(), 'activeTo' => null]
            );

        $newRestDay = new UserRestDay($this->getUser());
        $newRestDay->setRestDay($restDayApiModel->getRestDay());

        if ($restDay)
            {
            $restDay->setActiveTo($this->getSunday($restDay->getActiveFrom()));
            $newRestDay->setActiveFrom($this->getNextMondayFromDate($restDay->getActiveFrom()));
            $this->entityManager->persist($restDay);
            }

        $this->entityManager->persist($newRestDay);
        $this->entityManager->flush();

        return $this->createApiJsonResponse(
            self::SUCCESS_REST_DAY,
            'Rest day saved successfully',
            null,
            [
                'restDay' => $restDayApiModel->getRestDay(),
                'activeFrom' => $newRestDay->getActiveFrom()
            ]
        );
        }

    /**
     * @Route("/active_rest_day", name="get_active_rest_day", methods={"GET"})
     */
    public function getActiveRestDay()
        {
        $activeRestDay = $this->entityManager
            ->getRepository(UserRestDay::class)
            ->findActiveRestDay($this->getUser()->getId());

        if (!$activeRestDay)
            {
            return $this->createApiJsonResponse(
                self::ERROR_ACTIVE_REST_DAY,
                'You don\'t have a rest day'
            );
            }

        switch ($activeRestDay[0]['restDay'])
            {
            case 'Mon':
                $activeRestDay[0]['restDay'] = 'Monday';
                break;
            case 'Tue':
                $activeRestDay[0]['restDay'] = 'Tuesday';
                break;
            case 'Wed':
                $activeRestDay[0]['restDay'] = 'Wednesday';
                break;
            case 'Thu':
                $activeRestDay[0]['restDay'] = 'Thursday';
                break;
            case 'Fri':
                $activeRestDay[0]['restDay'] = 'Friday';
                break;
            case 'Sat':
                $activeRestDay[0]['restDay'] = 'Saturday';
                break;
            case 'Sun':
                $activeRestDay[0]['restDay'] = 'Sunday';
                break;
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_ACTIVE_REST_DAY,
            'Got active rest day successfully',
            null,
            [
                'restDay' => $activeRestDay[0]['restDay'],
                'activeFrom' => $activeRestDay[0]['activeFrom'],
                'activeTo' => $activeRestDay[0]['activeTo']
            ]
        );
        }

    /**
     * @Route("/all_rest_days", name="get_all_rest_days", methods={"GET"})
     */
    public function getAllRestDays()
        {
        $restDays = $this->entityManager
            ->getRepository(UserRestDay::class)
            ->getAllRestDays($this->getUser()->getId());

        if (!$restDays)
            {
            return $this->createApiJsonResponse(
                self::ERROR_REST_DAYS,
                'You don\'t have any rest days'
            );
            }

        foreach ($restDays as $key => $value)
            {
            switch ($restDays[$key]['restDay'])
                {
                case 'Mon':
                    $restDays[$key]['restDay'] = 'Monday';
                    break;
                case 'Tue':
                    $restDays[$key]['restDay'] = 'Tuesday';
                    break;
                case 'Wed':
                    $restDays[$key]['restDay'] = 'Wednesday';
                    break;
                case 'Thu':
                    $restDays[$key]['restDay'] = 'Thursday';
                    break;
                case 'Fri':
                    $restDays[$key]['restDay'] = 'Friday';
                    break;
                case 'Sat':
                    $restDays[$key]['restDay'] = 'Saturday';
                    break;
                case 'Sun':
                    $restDays[$key]['restDay'] = 'Sunday';
                    break;
                }
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_REST_DAYS,
            'Got rest days successfully',
            null,
            $restDays
        );
        }

    /**
     * @param DateTime $date
     * @return DateTime
     */
    private function getNextMondayFromDate(DateTime $date)
        {
        return $date->modify('next monday');
        }

    /**
     * @param DateTime $date
     * @return DateTime
     * @throws \Exception
     */
    private function getSunday(DateTime $date)
        {
        return new DateTime($date->modify('this sunday')->format('Y-m-d'));
        }
}