<?php


namespace App\Controller\Api;


use App\Controller\BaseController;
use App\Entity\User;
use App\Model\Api\RegistrationApiModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends BaseController
{
    /**
     * @Route("/api/register", name="api_register", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function registerUserAction(Request $request, \Swift_Mailer $mailer)
        {
        $registrationApiModel = $this->deserializeToJson(
            $request->getContent(),
            RegistrationApiModel::class
        );

        $violations = $this->validator->validate($registrationApiModel);

        if ($violations->count() > 0)
            {
            return $this->validationErrorResponse($violations, 400);
            }

        $user = $this->entityManager
            ->getRepository(User::class)
            ->findOneByUsernameOrEmail(
                $registrationApiModel->getUsername(),
                $registrationApiModel->getEmail()
            );

        if ($user)
            {
            return $this->createApiJsonResponse(
                self::ERROR_REGISTER,
                'User with this username or email already exists'
            );
            }

        $user = new User();
        $user->setUsername($registrationApiModel->getUsername());
        $user->setEmail($registrationApiModel->getEmail());
        $user->setPassword($this->passwordEncoder->encodePassword($user, $registrationApiModel->getPlainPassword()));
        $user->setRoles(["ROLE_API_USER"]);

        $this->entityManager->persist($user);
        $this->entityManager->flush();


        $message = (new \Swift_Message('Welcome to Workout Posse'))
            ->setFrom('workout.posse.team@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView('emails/registration.html.twig', [
                    'username' => $user->getUsername()
                ]),
                'text/html'
            )
            ->setReadReceiptTo(['workout.posse.team@gmail.com'])
        ;
        $mailer->send($message);

        $user = [
            'username' => $registrationApiModel->getUsername(),
            'password' => $registrationApiModel->getPlainPassword()
        ];

        return $this->createApiJsonResponse(
            self::SUCCESS_REGISTER,
            'User created successfully',
            $user
        );
        }
}