<?php


namespace App\Controller\Api;


use App\Controller\BaseController;
use App\Entity\Calendar;
use App\Entity\User;
use App\Entity\UserRestDay;
use App\Model\Api\CalendarApiModel;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CalendarController
 * @package App\Controller\Api
 * @Route("/api/auth/user")
 */
class CalendarController extends BaseController
{
    /**
     * @Route("/save_workout_day", name="api_new_workout_day", methods={"POST"})
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function addWorkoutDayAction(Request $request)
        {
        $calendarApiModel = $this->deserializeToJson(
            $request->getContent(),
            CalendarApiModel::class
        );

        $violations = $this->validator->validate($calendarApiModel);

        if ($violations->count() > 0)
            {
            return $this->validationErrorResponse($violations, 400);
            }

        $restDay = $this->entityManager
            ->getRepository(UserRestDay::class)
            ->findActiveRestDay($this->getUser()->getId());

        if (!$restDay)
            {
            return $this->createApiJsonResponse(
                self::ERROR_ACTIVE_REST_DAY,
                'Before saving your workout days you need to pick a rest day'
            );
            }

        if ($restDay[0]['restDay'] === $this->getToday('D'))
            {
            return $this->createApiJsonResponse(
                self::ERROR_REST_DAY,
                'This is your rest day so relax'
            );
            }

        $calendar = $this->entityManager
            ->getRepository(Calendar::class)
            ->findMatchingDate($this->getToday('Y-m-d'), $this->getUser());

        if ($calendar)
            {
            return $this->createApiJsonResponse(
                self::ERROR_WORKOUT_DAY,
                'Workout for this date already exists'
            );
            }

        $calendar = new Calendar($this->getUser());
        $calendar->setIsWorkoutDone($calendarApiModel->getIsWorkoutDone());
        $this->entityManager->persist($calendar);
        $this->entityManager->flush();

        $workoutDay = [
            'workoutDate' => $calendar->getWorkoutDate(),
            'isWorkoutDone' => $calendar->getIsWorkoutDone()
        ];

        return $this->createApiJsonResponse(
            self::SUCCESS_WORKOUT_DAY,
            'Workout day created successfully',
            null,
            $workoutDay
        );
        }

    /**
     * @Route("/get_today_workout", name="get_today_workut", methods={"GET"})
     */
    public function getTodayWorkout()
        {
        $calendar = $this->entityManager
            ->getRepository(Calendar::class)
            ->getTodayWorkout($this->getUser(), $this->getToday('Y-m-d'));

        if (!$calendar)
            {
            return $this->createApiJsonResponse(
                self::ERROR_TODAY_WORKOUT,
                'You don\'t have a workout day for today'
            );
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_TODAY_WORKOUT,
            'Got workout day for today',
            null,
            $calendar[0]
        );
        }

    /**
     * @Route("/user_calendar/{username}", name="api_get_user_calendar", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserCalendar(Request $request)
        {
        $user = $this->entityManager
            ->getRepository(User::class)
            ->getUserData($request->get('username'));

        if (!$user)
            {
            return $this->createApiJsonResponse(
                self::ERROR_USER_DATA,
                'This user does not exist'
            );
            }

        $calendar = $this->entityManager
            ->getRepository(Calendar::class)
            ->getUserCalendar($request->get('username'));

        if (!$calendar)
            {
            return $this->createApiJsonResponse(
                self::ERROR_GET_USER_CALENDAR,
                'User currently has no calendar data',
                $user
            );
            }

        return $this->createApiJsonResponse(
            self::SUCCESS_GET_USER_CALENDAR,
            'Got calendar data successfully',
            $user,
            $calendar
        );
        }

    /**
     * @Route("/workout_day_count", name="api_workout_date_count", methods={"GET"})
     * @throws \Exception
     */
    public function countWorkoutDates()
        {
        $calendar = $this->entityManager
            ->getRepository(Calendar::class)
            ->getUserCalendar($this->getUser());
        if (!$calendar)
            {
            return $this->createApiJsonResponse(
                self::CALENDAR_COUNT,
                'You don\' have any workout days'
            );
            }

        return $this->createApiJsonResponse(
            self::CALENDAR_COUNT,
            'Got workout days successfully',
            null,
            [
                'summary' => count($calendar),
            ]
        );
        }

    private function getToday(string $format)
        {
        $todayDate = new DateTime();
        return $todayDate->format($format);
        }
}