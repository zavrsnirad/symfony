<?php


namespace App\Controller;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseController extends AbstractController
{
    const SUCCESS_REST_DAY = 'successRestDay';
    const SUCCESS_REST_DAYS = 'successRestDays';
    const SUCCESS_ACTIVE_REST_DAY = 'successActiveRestDay';
    const SUCCESS_REGISTER = 'successRegister';
    const SUCCESS_IMAGE_UPLOAD = 'successImageUpload';
    const SUCCESS_USER_DATA = 'successUserData';
    const SUCCESS_CALENDAR = 'successCalendar';
    const SUCCESS_WORKOUT_DAY = 'successWorkoutDay';
    const SUCCESS_GET_USER_IMAGES = 'successGetUserImages';
    const SUCCESS_GET_USER_CALENDAR = 'successGetUserCalendar';
    const SUCCESS_CHANGE_PASSWORD = 'successChangePassword';
    const SUCCESS_TODAY_WORKOUT = 'successTodayWorkout';
    const SUCCESS_USER_SEARCH = 'successUserSearch';
    const SUCCESS_USER_FOLLOWING = 'successUserFollowing';
    const SUCCESS_USER_FOLLOWERS = 'successUserFollowers';
    const SUCCESS_USER_FOLLOWED = 'successUserFollowed';
    const SUCCESS_USER_UNFOLLOWED = 'successUserUnfollowed';
    const SUCCESS_HOME_IMAGES = 'successHomeImages';
    const SUCCESS_USER_PROFILE_IMAGE = 'successUserProfileImage';
    const SUCCESS_IMAGE_DELETED = 'successImageDeleted';

    const ERROR_REST_DAY = 'errorRestDay';
    const ERROR_REST_DAYS = 'errorRestDays';
    const ERROR_ACTIVE_REST_DAY = 'errorActiveRestDay';
    const ERROR_REGISTER = 'errorRegister';
    const ERROR_IMAGE_UPLOAD = 'errorImageUpload';
    const ERROR_USER_DATA = 'errorUserData';
    const ERROR_CALENDAR = 'errorCalendar';
    const ERROR_WORKOUT_DAY = 'errorWorkoutDay';
    const ERROR_GET_USER_IMAGES = 'errorGetUserImages';
    const ERROR_GET_USER_CALENDAR = 'errorGetUserCalendar';
    const ERROR_CHANGE_PASSWORD = 'errorChangePassword';
    const ERROR_TODAY_WORKOUT = 'errorTodayWorkout';
    const ERROR_USER_SEARCH = 'errorUserSearch';
    const ERROR_USER_FOLLOWING = 'errorUserFollowing';
    const ERROR_USER_FOLLOWERS = 'errorUserFollowers';
    const ERROR_USER_FOLLOWED = 'errorUserFollowed';
    const ERROR_USER_UNFOLLOWED = 'errorUserUnfollowed';
    const ERROR_HOME_IMAGES = 'errorHomeImages';
    const ERROR_USER_PROFILE_IMAGE = 'errorUserProfileImage';
    const ERROR_IMAGE_NOT_DELETED = 'errorImageNotDeleted';
    const ERROR_IMAGE_NOT_FOUND = 'errorImageNotFound';

    const CALENDAR_COUNT = 'calendarCount';
    const DELETE_USER = 'deleteUser';
    const MAIL_SENT = 'mailSent';

    public $serializer;
    public $validator;
    public $entityManager;
    public $passwordEncoder;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder)
        {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        }

    protected function createApiJsonResponse(string $type, string $message, array $userData = null, array $data = null)
        {
        return new JsonResponse([
            'type' => $type,
            'message' => $message,
            'userData' => $userData,
            'data' => $data
        ]);
        }

    protected function validationErrorResponse($data, $statusCode)
        {
        $jsonData = $this->json($data);
        return new Response($jsonData,
            $statusCode,
            ['Content-Type' => 'application/json']
        );
        }

    protected function deserializeToJson($data, string $className)
        {
        return $this->serializer->deserialize(
            $data,
            $className,
            'json'
        );
        }
}