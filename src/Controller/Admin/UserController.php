<?php


namespace App\Controller\Admin;


use App\Controller\BaseAdminController;
use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller\Admin
 * @Route("/admin")
 */
class UserController extends BaseAdminController
{
    protected function updateEntity($entity)
        {
        if ($entity instanceof User)
            {
            $username = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneBy(['username' => $entity->getUsername()]);

            $email = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneBy(['email' => $entity->getEmail()]);

            if (!$username || !$email)
                {
                parent::updateEntity($entity);
                $this->addFlash('success', 'User credentials updated');
                return $this->redirectToAdminEntity('User');
                }

            $this->addFlash('error', 'User not updated, username or email already exist');
            return $this->redirectToAdminEntity('User');
            }

        $this->addFlash('error', 'Not user instance');
        return $this->redirectToAdminEntity('User');
        }
}