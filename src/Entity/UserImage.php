<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Description:
 * Upload image json:
 * {
 *      "imageFileName": "image.jpeg",
 *      "encodedImageData": "Probably something very long cause it's Base64 encoded",
 *      "isProfileImage": true/false/1/0
 * }
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserImageRepository")
 * @ORM\Table(name="user_image")
 */
class UserImage
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $imageFileName;

    /**
     * @ORM\Column(type="string")
     */
    private $originalImageFileName;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isProfileImage = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isOldProfileImage = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userImages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct(User $user)
        {
        $this->user = $user;
        }

    public function getId(): int
        {
        return $this->id;
        }

    public function setId($id): void
        {
        $this->id = $id;
        }

    public function getImageFileName(): string
        {
        return $this->imageFileName;
        }

    public function setImageFileName($imageFileName): void
        {
        $this->imageFileName = $imageFileName;
        }

    public function getOriginalImageFileName(): string
        {
        return $this->originalImageFileName;
        }

    public function setOriginalImageFileName($originalImageFileName): void
        {
        $this->originalImageFileName = $originalImageFileName;
        }

    public function getIsProfileImage()
        {
        return $this->isProfileImage;
        }

    public function setIsProfileImage($isProfileImage): void
        {
        $this->isProfileImage = $isProfileImage;
        }

    public function getIsOldProfileImage()
        {
        return $this->isOldProfileImage;
        }

    public function setIsOldProfileImage($isOldProfileImage): void
        {
        $this->isOldProfileImage = $isOldProfileImage;
        }

    public function getDescription()
        {
        return $this->description;
        }

    public function setDescription($description): void
        {
        $this->description = $description;
        }

    public function getUser()
        {
        return $this->user;
        }
}