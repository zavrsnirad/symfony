<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Description:
 * Login json:
 * {
 *      "username": "username",
 *      "password": "password"
 * }
 * Register json:
 * {
 *      "username": "username",
 *      "email":    "email@example.com",
 *      "plainPassword": "plainPassword",
 *      "plainConfirmPassword": "plainConfirmPassword"
 * }
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserImage", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userImages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserRestDay", mappedBy="user", cascade={"persist", "remove"})
     */
    private $restDay;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Calendar", mappedBy="user", cascade={"persist", "remove"})
     */
    private $calendar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserFollowing", mappedBy="userFollowing", cascade={"persist", "remove"})
     */
    private $userFollowing;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserFollowing", mappedBy="userFollowed", cascade={"persist", "remove"})
     */
    private $userFollowed;

    public function __construct()
        {
        $this->userImages = new ArrayCollection();
        $this->calendar = new ArrayCollection();
        }

    public function getId(): ?int
        {
        return $this->id;
        }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
        {
        return (string) $this->username;
        }

    public function setUsername(string $username): self
        {
        $this->username = $username;

        return $this;
        }

    public function getEmail(): string
        {
        return (string) $this->email;
        }

    public function setEmail($email): self
        {
        $this->email = $email;
        return $this;
        }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
        {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
        }

    public function setRoles(array $roles): self
        {
        $this->roles = $roles;

        return $this;
        }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
        {
        return (string) $this->password;
        }

    public function setPassword(string $password): self
        {
        $this->password = $password;

        return $this;
        }

    /**
     * @see UserInterface
     */
    public function getSalt()
        {
        // not needed when using the "bcrypt" algorithm in security.yaml
        }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
        {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
        }

    /**
     * @return Collection|UserImage[]
     */
    public function getUserImages(): Collection
        {
        return $this->userImages;
        }

    public function getRestDay()
        {
        return $this->restDay;
        }

    /**
     * @param mixed $restDay
     */
    public function setRestDay($restDay): void
        {
        $this->restDay = $restDay;
        }

    /**
     * @return mixed
     */
    public function getCalendar()
        {
        return $this->calendar;
        }

    /**
     * @param mixed $calendar
     */
    public function setCalendar($calendar): void
        {
        $this->calendar = $calendar;
        }

    public function __toString()
        {
        return $this->getEmail();
        }
}
