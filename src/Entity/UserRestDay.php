<?php


namespace App\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Description:
 * Save rest day:
 * {
 *      "restDay": "Mon/Tue/Wed/Thu/Fri/Sat/Sun"
 * }
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRestDayRepository")
 * @ORM\Table(name="user_rest_day")
 */
class UserRestDay
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $restDay;

    /**
     * @ORM\Column(type="date")
     */
    private $activeFrom;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $activeTo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="restDay")
     */
    private $user;

    public function __construct(User $user)
        {
        $this->user = $user;
        $this->activeFrom = new DateTime();
        }

    public function getId()
        {
        return $this->id;
        }

    public function getRestDay()
        {
        return $this->restDay;
        }

    public function setRestDay($restDay): void
        {
        $this->restDay = $restDay;
        }

    public function getActiveFrom()
        {
        return $this->activeFrom;
        }

    public function setActiveFrom($activeFrom): void
        {
        $this->activeFrom = $activeFrom;
        }

    public function getActiveTo()
        {
        return $this->activeTo;
        }

    public function setActiveTo($activeTo): void
        {
        $this->activeTo = $activeTo;
        }

    public function getUser()
        {
        return $this->user;
        }
}