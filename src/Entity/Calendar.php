<?php


namespace App\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Description:
 * Save workout date json:
 * {
 *      "isWorkoutDone": true/false/1/0
 * }
 *
 * @ORM\Entity(repositoryClass="App\Repository\CalendarRepository")
 * @ORM\Table(name="calendar")
 */
class Calendar
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $workoutDate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isWorkoutDone;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="calendar")
     */
    private $user;

    public function __construct(User $user)
        {
        $this->user = $user;
        $this->workoutDate = new DateTime();
        }

    public function getId()
        {
        return $this->id;
        }

    public function getWorkoutDate()
        {
        return $this->workoutDate;
        }

    public function setWorkoutDate($workoutDate): void
        {
        $this->workoutDate = $workoutDate;
        }

    public function getIsWorkoutDone()
        {
        return $this->isWorkoutDone;
        }

    public function setIsWorkoutDone($isWorkoutDone): void
        {
        $this->isWorkoutDone = $isWorkoutDone;
        }

    public function getUser()
        {
        return $this->user;
        }
}