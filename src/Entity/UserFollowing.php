<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserFollowingRepository")
 * @ORM\Table(name="user_following")
 */
class UserFollowing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userFollowing")
     */
    private $userFollowing;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userFollowed")
     */
    private $userFollowed;

    public function getId()
        {
        return $this->id;
        }

    public function getUserFollowing()
        {
        return $this->userFollowing;
        }

    public function setUserFollowing($userFollowing): void
        {
        $this->userFollowing = $userFollowing;
        }

    public function getUserFollowed()
        {
        return $this->userFollowed;
        }

    public function setUserFollowed($userFollowed): void
        {
        $this->userFollowed = $userFollowed;
        }
}