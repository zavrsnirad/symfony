<?php


namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class UserFollowingRepository extends EntityRepository
{
    public function getFollowing(int $userId)
        {
        return $this->createQueryBuilder('uf')
            ->select('IDENTITY(uf.userFollowed) as userFollowed', 'user.username AS userFollowing')
            ->innerJoin('uf.userFollowed', 'user')
            ->andWhere('uf.userFollowing = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
        }

    public function getFollowers(int $userId)
        {
        return $this->createQueryBuilder('uf')
            ->select('IDENTITY(uf.userFollowed) as userFollowed', 'user.username AS userFollowing')
            ->innerJoin('uf.userFollowing', 'user')
            ->andWhere('uf.userFollowed = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
        }
}