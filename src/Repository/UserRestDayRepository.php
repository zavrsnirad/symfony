<?php


namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class UserRestDayRepository extends EntityRepository
{
    public function findActiveRestDay(int $userId)
        {
        $date = new \DateTime();
        $today = $date->format('Y-m-d');
        return $this->createQueryBuilder('urd')
            ->select('urd.restDay', 'urd.activeFrom', 'urd.activeTo')
            ->andWhere('urd.user = :user AND (:today BETWEEN urd.activeFrom AND urd.activeTo OR urd.activeTo IS NULL)')
            ->setParameter('user', $userId)
            ->setParameter('today', $today)
            ->getQuery()
            ->getResult()
            ;
        }

    public function getAllRestDays(int $userId)
        {
        $date = new \DateTime();
        $today = $date->format('Y-m-d');
        return $this->createQueryBuilder('urd')
            ->select('urd.restDay', 'urd.activeFrom', 'urd.activeTo')
            ->andWhere('urd.user = :user')
            ->andWhere('urd.activeFrom > :today')
            ->setParameter('user', $userId)
            ->setParameter('today', $today)
            ->orderBy('urd.activeFrom', 'ASC')
            ->getQuery()
            ->getResult()
            ;
        }
}