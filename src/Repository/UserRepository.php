<?php


namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findOneByUsernameOrEmail(string $username, string $email)
        {
        return $this->createQueryBuilder('user')
            ->orWhere('user.username = :username')
            ->setParameter('username', $username)
            ->orWhere('user.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getResult();
        }

    public function getLoginUserData(string $username)
        {
        return $this->createQueryBuilder('user')
            ->select('user.id AS id, user.email AS email')
            ->andWhere('user.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getResult();
        }

    public function getUserData(string $username)
        {
        return $this->createQueryBuilder('user')
            ->select('user.id AS userId', 'user.username AS username')
            ->andWhere('user.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getResult();
        }

    public function searchUserIds(string $username, string $userSearching)
        {
        return $this->createQueryBuilder('user')
            ->select('user.id')
            ->andWhere('user.username LIKE :username AND user.username != :userSearching')
            ->setParameter('username', '%'.$username.'%')
            ->setParameter('userSearching', $userSearching)
            ->getQuery()
            ->getResult();
        }

    public function searchUsersByIds(array $ids)
        {
        $qb = $this->createQueryBuilder('user');
        $qb->select('user.id', 'user.username');
        $qb->addSelect('uim.imageFileName');
        $qb->leftJoin('user.userImages', 'uim', 'with', 'uim.isProfileImage = :isProfileImage');
        $qb->add('where', $qb->expr()->in('user.id', ':ids'));
        $qb->setParameter('ids', $ids);
        $qb->setParameter('isProfileImage', 1);
        return $qb->getQuery()->getResult();
        }
}