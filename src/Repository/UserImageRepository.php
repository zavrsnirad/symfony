<?php


namespace App\Repository;


use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserImageRepository extends EntityRepository
{
    public function getAllUserImages(string $username)
        {
        return $this->createQueryBuilder('ui')
            ->select(
                'ui.imageFileName',
                'ui.description',
                'ui.createdAt'
            )
            ->innerJoin('ui.user','u')
            ->andWhere('u.username = :username 
                        AND ui.isOldProfileImage = :isOldProfileImage 
                        AND ui.isProfileImage = :isProfileImage')
            ->setParameter('username', $username)
            ->setParameter('isOldProfileImage', 0)
            ->setParameter('isProfileImage', 0)
            ->orderBy('ui.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
        }

    public function getUserProfileImage(string $username)
        {
        return $this->createQueryBuilder('ui')
            ->select('ui.imageFileName')
            ->innerJoin('ui.user', 'u')
            ->andWhere('u.username = :username 
                        AND ui.isProfileImage = :isProfileImage')
            ->setParameter('username', $username)
            ->setParameter('isProfileImage', 1)
            ->getQuery()
            ->getResult()
        ;
        }

    public function getFollowingUserImages(array $ids)
        {
        $qb = $this->createQueryBuilder('ui');
        $qb->select('u.id', 'u.username', 'ui.imageFileName', 'ui.description');
        $qb->leftJoin('ui.user', 'u');
        $qb->add('where', $qb->expr()->in('u.id', ':ids'));
        $qb->andWhere('ui.isProfileImage = 0 AND ui.isOldProfileImage = 0');
        $qb->setParameter('ids', $ids);
        $qb->orderBy('ui.createdAt', 'DESC');
        return $qb->getQuery()->getResult();
        }

    public function getFollowedUserProfileImage(array $ids)
        {
        $qb = $this->createQueryBuilder('ui');
        $qb->select('u.id', 'u.username', 'ui.imageFileName');
        $qb->innerJoin('ui.user', 'u');
        $qb->add('where', $qb->expr()->in('u.id', ':ids'));
        $qb->andWhere('ui.isProfileImage = 1');
        $qb->setParameter('ids', $ids);
        return $qb->getQuery()->getResult();
        }
}