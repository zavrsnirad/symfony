<?php


namespace App\Repository;


use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class CalendarRepository extends EntityRepository
{
    public function findMatchingDate(string $date, User $user)
        {
        return $this->createQueryBuilder('calendar')
            ->andWhere('calendar.workoutDate = :workoutDate AND calendar.user = :user')
            ->setParameter('workoutDate', $date)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
        }

    public function getUserCalendar(string $username)
        {
        return $this->createQueryBuilder('c')
            ->select('c.workoutDate', 'c.isWorkoutDone')
            ->innerJoin('c.user', 'u')
            ->andWhere('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getResult()
        ;
        }

    public function getTodayWorkout(User $user, string $today)
        {
        return $this->createQueryBuilder('calendar')
            ->select('calendar.workoutDate', 'calendar.isWorkoutDone')
            ->andWhere('calendar.user = :user AND calendar.workoutDate = :today')
            ->setParameter('user', $user)
            ->setParameter('today', $today)
            ->getQuery()
            ->getResult()
            ;
        }
}